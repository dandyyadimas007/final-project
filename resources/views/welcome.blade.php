<!DOCTYPE html>
<html lang="en">

<head>

  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="author" content="">

  <title>Sosial Media Sanbook</title>

  <!-- Bootstrap core CSS -->
  <link href="{{ asset('/home/vendor/bootstrap/css/bootstrap.min.css')}}" rel="stylesheet">

  <!-- Custom fonts for this template -->
  <link href="{{ asset('/home/vendor/fontawesome-free/css/all.min.css')}}" rel="stylesheet">
  <link rel="stylesheet" href="{{ asset('/home/vendor/simple-line-icons/css/simple-line-icons.css')}}">
  <link href="https://fonts.googleapis.com/css?family=Lato" rel="stylesheet">
  <link href="https://fonts.googleapis.com/css?family=Catamaran:100,200,300,400,500,600,700,800,900" rel="stylesheet">
  <link href="https://fonts.googleapis.com/css?family=Muli" rel="stylesheet">

  <!-- Plugin CSS -->
  <link rel="stylesheet" href="{{ asset('/home/device-mockups/device-mockups.min.css')}}">

  <!-- Custom styles for this template -->
  <link href="{{ asset('/home/css/new-age.min.css')}}" rel="stylesheet">

</head>

<body id="page-top">

  <!-- Navigation -->
 @include('sanbook.partials.navbar')
  <!-- /.Navigation --> 

  <!-- content header -->
  @include('sanbook.partials.header')
   <!-- /.content header -->


  <!-- Bootstrap core JavaScript -->
  <script src="{{ asset('/home/vendor/jquery/jquery.min.js')}}"></script>
  <script src="{{ asset('/home/vendor/bootstrap/js/bootstrap.bundle.min.js')}}"></script>

  <!-- Plugin JavaScript -->
  <script src="{{ asset('/home/vendor/jquery-easing/jquery.easing.min.js')}}"></script>

  <!-- Custom scripts for this template -->
  <script src="{{ asset('/home/js/new-age.min.js')}}"></script>

</body>

</html>
