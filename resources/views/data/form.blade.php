<!DOCTYPE html>
<html lang="en">

<head>

  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="author" content="">

  <title>New Age - Start Bootstrap Theme</title>

  <!-- Bootstrap core CSS -->
  <link href="{{ asset('/home/vendor/bootstrap/css/bootstrap.min.css')}}" rel="stylesheet">

  <!-- Custom fonts for this template -->
  <link href="{{ asset('/home/vendor/fontawesome-free/css/all.min.css')}}" rel="stylesheet">
  <link rel="stylesheet" href="{{ asset('/home/vendor/simple-line-icons/css/simple-line-icons.css')}}">
  <link href="https://fonts.googleapis.com/css?family=Lato" rel="stylesheet">
  <link href="https://fonts.googleapis.com/css?family=Catamaran:100,200,300,400,500,600,700,800,900" rel="stylesheet">
  <link href="https://fonts.googleapis.com/css?family=Muli" rel="stylesheet">

  <!-- Plugin CSS -->
  <link rel="stylesheet" href="{{ asset('/home/device-mockups/device-mockups.min.css')}}">

  <!-- Custom styles for this template -->
  <link href="{{ asset('/home/css/new-age.min.css')}}" rel="stylesheet">

</head>

<body id="page-top">
  <header class="masthead">
    <div class="container h-100">
      <div class="row h-100">
        <div class="col-lg-7 my-auto">
          <div class="header-content mx-auto">
            <h1 class="mb-5">Selamat Datang di Sosial Media Sanbook</h1>
          </div>
        </div>

        <!-- Formulir mulai dari sini -->
        <div class="col-lg-5 my-auto">
          <form action="/signup" method="POST">
            @csrf
               <label for="first">First Name</label><br>
               <input type="text" placeholder="first name" id="first" name="first" required><br>
               <label for="last">Last Name</label><br>
               <input type="text" placeholder="last name" id="last" name="last" required><br>
               <label for="email">Email</label><br> 
               <input type="text" placeholder="Email" id="email" name="email" required><br>
               <label for="username">Username</label><br>
               <input type="text" placeholder="username" id="username" name="username" required><br>
               <label for="password">Password</label><br>
               <input type="password" placeholder="password" id="password" name="password" required><br><br>
               <input type="submit" value="Sign Up">
           </form>
        </div>
        <!-- Formulir Berakhir disini -->
      </div>
    </div>
  </header>


  <!-- Bootstrap core JavaScript -->
  <script src="{{ asset('/home/vendor/jquery/jquery.min.js')}}"></script>
  <script src="{{ asset('/home/vendor/bootstrap/js/bootstrap.bundle.min.js')}}"></script>

  <!-- Plugin JavaScript -->
  <script src="{{ asset('/home/vendor/jquery-easing/jquery.easing.min.js')}}"></script>

  <!-- Custom scripts for this template -->
  <script src="{{ asset('/home/js/new-age.min.js')}}"></script>

</body>

</html>
    